import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/MUO/python/muCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class muSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.isMC:
            if not isUL and self.year <= 2018:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_muSF"):
                os.environ["_muSF"] = "_muSF"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")
                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

                # Declaring two objects, one for tightID and the other for tightRelIso.
                # Of course this should be changed if more WPs are needed, maybe looping over 
                # an input parameter.
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tightid = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrNameID"]))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tightreliso = MyCorrections("%s", "%s");' %
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrNameIso"]))

                # alternating two different versions of the get_sf functions to comply with 
                # the two different schema of the json (i.e. w/ or w/o "year" to evaluate)
                if self.year <= 2018:
                    ROOT.gInterpreter.Declare("""
                        using Vfloat = const ROOT::RVec<float>&;
                        using Vint = const ROOT::RVec<int>&;
                        ROOT::RVec<double> get_mu_tight_id_sf(std::string syst, Vfloat eta, Vfloat pt) {
                            ROOT::RVec<double> sf;
                            for (size_t i = 0; i < pt.size(); i++) {
                                if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                                else sf.push_back(corr_tightid.eval({"%s", fabs(eta[i]), pt[i], syst}));
                            }
                            return sf;
                        }
                        ROOT::RVec<double> get_mu_tight_iso_sf(std::string syst, Vfloat eta, Vfloat pt) {
                            ROOT::RVec<double> sf;
                            for (size_t i = 0; i < pt.size(); i++) {
                                if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                                else sf.push_back(corr_tightreliso.eval({"%s", fabs(eta[i]), pt[i], syst}));
                            }
                            return sf;
                        }
                    """  % (corrCfg[self.corrKey]["yearkey"], corrCfg[self.corrKey]["yearkey"]))

                else:
                    ROOT.gInterpreter.Declare("""
                        using Vfloat = const ROOT::RVec<float>&;
                        using Vint = const ROOT::RVec<int>&;
                        ROOT::RVec<double> get_mu_tight_id_sf(std::string syst, Vfloat eta, Vfloat pt) {
                            ROOT::RVec<double> sf;
                            for (size_t i = 0; i < pt.size(); i++) {
                                if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                                else sf.push_back(corr_tightid.eval({fabs(eta[i]), pt[i], syst}));
                            }
                            return sf;
                        }
                        ROOT::RVec<double> get_mu_tight_iso_sf(std::string syst, Vfloat eta, Vfloat pt) {
                            ROOT::RVec<double> sf;
                            for (size_t i = 0; i < pt.size(); i++) {
                                if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                                else sf.push_back(corr_tightreliso.eval({fabs(eta[i]), pt[i], syst}));
                            }
                            return sf;
                        }
                    """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for corr in self.wps:
            # comply with different naming of central sf value in Run2 and Run3
            if self.year <= 2018: systematics = [("", "sf"), ("_up", "systup"), ("_down", "systdown")]
            else:                 systematics = [("", "nominal"), ("_up", "systup"), ("_down", "systdown")]

            for syst_name, syst in systematics:
                df = df.Define("musf_%s%s" % (corr, syst_name),
                                'get_mu_%s_sf("%s", Muon_eta, Muon_pt)' % (corr, syst))
                
                branches.append("musf_%s%s" % (corr, syst_name))

        return df, branches


def muSFRDF(**kwargs):
    """
    Module to obtain muon SFs with their uncertainties.

    :param wps: name of the wps to consider among ``tight_id``, ``tight_iso``.
        Note: probably more can be considered.
    :type wps: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: muSFRDF
            path: Corrections.MUO.muCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
                isUL: self.dataset.has_tag('ul')
                wps: [tight_id, tight_iso]
    """
    return lambda: muSFRDFProducer(**kwargs)

