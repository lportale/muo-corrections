import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.MUO.muCorrections import muSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def muon_test(df, isMC, year, runPeriod):
    musf = muSFRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        wps=["tight_id", "tight_iso"]
    )()
    df, _ = musf.run(df)
    h = df.Histo1D("musf_tight_id")
    print(f"Muon {year}{runPeriod} tightID SF Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df


if __name__ == "__main__":
    df_mc2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    _ = muon_test(df_mc2022, True, 2022, "preEE")

    df_mc2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    _ = muon_test(df_mc2022_postEE, True, 2022, "postEE")

    df_mc2023 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023.root")))
    _ = muon_test(df_mc2023, True, 2023, "preBPix")

    df_mc2023_postBpix = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023_postbpix.root")))
    _ = muon_test(df_mc2023_postBpix, True, 2023, "postBPix")